package com.webappmate.formsproject.Fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.webappmate.formsproject.Adapter.StatusAdapter;
import com.webappmate.formsproject.FormApplication.FormActivity;
import com.webappmate.formsproject.Internet.CheckInternet;
import com.webappmate.formsproject.Listener.RecycleListener;
import com.webappmate.formsproject.Model.formStatusModel;
import com.webappmate.formsproject.R;

import java.util.ArrayList;
import java.util.Map;


public class FormWorking extends Fragment {
    View view;
    formStatusModel statusModel;
    StatusAdapter statusAdapter;
    DatabaseReference mDatabase;
    LinearLayout linearLayout;
    boolean oneTime = true;
    TableLayout.LayoutParams layoutParams;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        try {
            if (isVisibleToUser) {
                internet();
                initViews();
                linearLayout.removeAllViews();
                fetchData();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.setUserVisibleHint(isVisibleToUser);
    }

    LinearLayout.LayoutParams lparams;
    private ProgressDialog progressDialog;
    private ArrayList<formStatusModel> list = new ArrayList();

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_form_working, container, false);
        linearLayout = (LinearLayout) view.findViewById(R.id.layout_working);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        return view;
    }

    private void textans(String ans, TableRow tableRow) {
        TextView answ = new TextView(getContext());
        answ.setText(ans);
        answ.setSingleLine(false);
        answ.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
        tableRow.addView(answ);
    }

    private void textquest(String ques, TableRow tableRow) {
        TextView quest = new TextView(getContext());
        quest.setTypeface(Typeface.defaultFromStyle(1));
        quest.setTypeface(null, Typeface.BOLD);
        quest.setText(ques + " : ");
        tableRow.addView(quest);
    }

    private void initViews() {
        mDatabase = FirebaseDatabase.getInstance().getReference();
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Please wait..");
        progressDialog.show();
        layoutParams = new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(10, 5, 10, 5);
    }

    private void fetchData() {

        mDatabase.child("Client").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Map<String, String> map = (Map<String, String>) dataSnapshot.getValue();
                int formstatus = Integer.parseInt(map.get("status"));
                if (formstatus == 1) {
                    String key_client = dataSnapshot.getKey();
                    CardView cardView = new CardView(getContext());

                    LinearLayout.LayoutParams cvlayoutParams = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    cvlayoutParams.setMargins(10, 10, 10, 10);

                    cardView.setLayoutParams(cvlayoutParams);
                    final TableLayout tableLayout = new TableLayout(getContext());
                    linearLayout.addView(cardView);
                    cardView.addView(tableLayout);
                    mDatabase.child("Client").child(key_client).addChildEventListener(new ChildEventListener() {
                        @Override
                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                            try {
                                Map<String, String> map = (Map<String, String>) dataSnapshot.getValue();
                                TableRow tableRow = new TableRow(getContext());
                                tableRow.setOrientation(LinearLayout.HORIZONTAL);
                                tableRow.setLayoutParams(layoutParams);
                                tableLayout.addView(tableRow);
                                String ques = map.get("Question");
                                textquest(ques, tableRow);
                                String ans = map.get("answer");
                                textans(ans, tableRow);
                                progressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                            progressDialog.dismiss();
                        }

                        @Override
                        public void onChildRemoved(DataSnapshot dataSnapshot) {
                            progressDialog.dismiss();
                        }

                        @Override
                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                            progressDialog.dismiss();

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            progressDialog.dismiss();

                        }
                    });


                } else {
                    progressDialog.dismiss();
                }
            }





            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                progressDialog.dismiss();

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                progressDialog.dismiss();

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                progressDialog.dismiss();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                progressDialog.dismiss();
                Toast.makeText(getContext(), "Error:"+databaseError, Toast.LENGTH_LONG).show();
            }
        });
    }

    public void internet() {
        if (!CheckInternet.isNetwork(getContext())) {

            Toast.makeText(getContext(), "There is no Internet Connection", Toast.LENGTH_LONG).show();
            progressDialog.dismiss();
        }
    }
}
