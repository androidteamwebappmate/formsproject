package com.webappmate.formsproject.Model;

/**
 * Created by newuser on 23/1/17.
 */

public class formStatusModel {
    String clientName;
    String clientEmail;
    String clientAddress;
    String clientMobile;
    String createdDate;

    public String getFormId() {
        return formId;
    }

    public void setFormId(String formId) {
        this.formId = formId;
    }

    String formId;

    public formStatusModel(String clientName, String clientEmail, String clientMobile, String clientAddress, String createdDate,String formId) {
        this.clientName=clientName;
        this.clientEmail=clientEmail;
        this.clientMobile=clientMobile;
        this.clientAddress=clientAddress;
        this.createdDate=createdDate;
        this.formId=formId;
    }

    public String getClientEmail() {
        return clientEmail;
    }

    public void setClientEmail(String clientEmail) {
        this.clientEmail = clientEmail;
    }

    public String getClientAddress() {
        return clientAddress;
    }

    public void setClientAddress(String clientAddress) {
        this.clientAddress = clientAddress;
    }

    public String getClientMobile() {
        return clientMobile;
    }

    public void setClientMobile(String clientMobile) {
        this.clientMobile = clientMobile;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }
}
