package com.webappmate.formsproject.Model;

/**
 * Created by newuser on 24/1/17.
 */

public class OptionItem {

    private String key,value;

    public OptionItem() {
        key="";
        value="";
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
