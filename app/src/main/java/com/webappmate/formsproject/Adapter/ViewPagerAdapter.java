package com.webappmate.formsproject.Adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import com.webappmate.formsproject.Fragment.FormAssigned;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by newuser on 23/1/17.
 */

public class ViewPagerAdapter extends FragmentPagerAdapter {
    Bundle bundle=new Bundle();
// Fragment formAssigned=new FormAssigned();
    int i;
    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();
    private FragmentManager mFragmentManager;
    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
        this.mFragmentManager=fm;
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }
    public void addFragment(Fragment fragment, String title) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Object object = super.instantiateItem(container, position);
        if (object instanceof Fragment) {
            Fragment fragment = (Fragment) object;
            String tag = fragment.getTag();
            mFragmentTitleList.add(position, tag);
        }
        return object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }
}
