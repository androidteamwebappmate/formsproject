package com.webappmate.formsproject.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TableLayout;
import android.widget.TextView;

import com.webappmate.formsproject.Listener.RecycleListener;
import com.webappmate.formsproject.Model.formStatusModel;
import com.webappmate.formsproject.R;

import java.util.ArrayList;
import java.util.zip.Inflater;

import static android.R.attr.typeface;

/**
 * Created by newuser on 23/1/17.
 */

public class StatusAdapter extends RecyclerView.Adapter<StatusAdapter.ViewHolder>{
    Context context;
    private RecycleListener clicklistener = null;
    ArrayList<formStatusModel> mDataset;
    public StatusAdapter(Context context, ArrayList<formStatusModel> mDataset) {
        this.context = context;
        this.mDataset = mDataset;
    }



    @Override
    public StatusAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_forms,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(StatusAdapter.ViewHolder holder, int position) {
        holder.txt_clientName.setText(mDataset.get(position).getClientName());
        holder.txt_clientEmail.setText(mDataset.get(position).getClientEmail());
        holder.txt_clientMobile.setText(mDataset.get(position).getClientMobile());
        holder.txt_clientAddress.setText(mDataset.get(position).getClientAddress());
        holder.txt_clientCreatedADate.setText(mDataset.get(position).getCreatedDate());


    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView txt_clientName,txt_clientEmail,txt_clientMobile,txt_clientAddress,txt_clientCreatedADate ;
        private TableLayout tablelayout;

        public ViewHolder(View itemView) {
            super(itemView);
            txt_clientName=(TextView)itemView.findViewById(R.id.txt_clientName);
            txt_clientEmail=(TextView)itemView.findViewById(R.id.txt_clientEmail);
            txt_clientMobile=(TextView)itemView.findViewById(R.id.txt_clientMobile);
            txt_clientAddress=(TextView)itemView.findViewById(R.id.txt_clientAddress);
            txt_clientCreatedADate=(TextView)itemView.findViewById(R.id.txt_clientCreatedADate);
            tablelayout=(TableLayout)itemView.findViewById(R.id.tablayout_formtype);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            if (clicklistener != null) {
                clicklistener.itemClicked(view, getAdapterPosition());
            }
        }
    }
    public void addItem(formStatusModel model) {
        mDataset.clear();
        mDataset.add(model);
        notifyDataSetChanged();
    }
    public void setClickListener(RecycleListener clicklistener) {
        this.clicklistener = clicklistener;
    }

}
