package com.webappmate.formsproject.FormApplication;

import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.webappmate.formsproject.Adapter.ViewPagerAdapter;
import com.webappmate.formsproject.Fragment.FormAssigned;
import com.webappmate.formsproject.Fragment.FormSubmitted;
import com.webappmate.formsproject.Fragment.FormWorking;
import com.webappmate.formsproject.R;

public class FormType extends AppCompatActivity implements TabLayout.OnTabSelectedListener, ViewPager.OnPageChangeListener {
    protected ViewPager viewPager;
    protected TabLayout tabsLayout;
    private ViewPagerAdapter viewPagerAdapter;
    private FragmentManager fm;
    Bundle bundle = new Bundle();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_type);
        initViews();
    }

    private void initViews() {

        viewPager = (ViewPager) findViewById(R.id.viewpager_formtype);
        tabsLayout = (TabLayout) findViewById(R.id.tablayout_formtype);
        setupViewPager(viewPager);
        tabsLayout.setupWithViewPager(viewPager);
        tabsLayout.setOnTabSelectedListener(this);
        viewPager.addOnPageChangeListener(this);

    }

    private void setupViewPager(ViewPager viewPager) {
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragment(new FormAssigned(), "Assigned");
        viewPagerAdapter.addFragment(new FormWorking(), "Working");
        viewPagerAdapter.addFragment(new FormSubmitted(), "Submitted");
        viewPager.setAdapter(viewPagerAdapter);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
