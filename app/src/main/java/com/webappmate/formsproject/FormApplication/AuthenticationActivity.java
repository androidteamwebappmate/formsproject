package com.webappmate.formsproject.FormApplication;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.webappmate.formsproject.R;

import io.fabric.sdk.android.Fabric;

import org.w3c.dom.Text;

public class AuthenticationActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText et_email, et_password;
    private Button btn_login;
    FirebaseAuth mAuth;
    private Switch btn_switch;
    private TextView forgot;
    private FirebaseUser user;
    private String email, password, btntxt, forgot_email;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_authentication);
        initViews();
        btn_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    et_password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
                } else {
                    et_password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);

                }
            }
        });
        btn_login.setOnClickListener(this);
        forgot.setOnClickListener(this);
    }

    private void signin(String email, String password) {

        progressDialog.setMessage("Please Wait..");
        progressDialog.show();
        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(AuthenticationActivity.this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (!task.isSuccessful()) {
                    progressDialog.dismiss();
                    String toast=task.getException().toString();
                  String  stoast=toast.replace("com.google.firebase.auth.FirebaseAuth","\n Something went wrong : ");
                    Toast.makeText(AuthenticationActivity.this, "Login Failed"+stoast, Toast.LENGTH_LONG).show();

                } else {
                    progressDialog.dismiss();
                    Intent formtype_intent=new Intent(AuthenticationActivity.this,FormType.class);
                    startActivity(formtype_intent);
                    Toast.makeText(AuthenticationActivity.this, "Logged in Successfully", Toast.LENGTH_LONG).show();
                    et_email.setText("");
                    et_password.setText("");
                }
            }
        });
    }

    private void initViews() {
        et_email = (EditText) findViewById(R.id.et_email);
        et_password = (EditText) findViewById(R.id.et_password);
        btn_login = (Button) findViewById(R.id.btn_login);
        btn_switch = (Switch) findViewById(R.id.switch_show);
        forgot = (TextView) findViewById(R.id.txt_forgot);
        mAuth = FirebaseAuth.getInstance();
        progressDialog = new ProgressDialog(this);
        forgot.setPaintFlags(forgot.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_login:
                try {

                    email = et_email.getText().toString();
                    password = et_password.getText().toString();
                    btntxt = btn_login.getText().toString();

                    if (btntxt == "Reset") {
                        if (email.equals("")) {
                            Toast.makeText(AuthenticationActivity.this, "Please enter Email", Toast.LENGTH_LONG).show();
                        } else if (!email.contains("@") || !email.contains(".")) {
                            Toast.makeText(AuthenticationActivity.this, "Please enter valid Email", Toast.LENGTH_LONG).show();
                        }else
                        forgotpassword(email);
                    }  else {
                        if (email.equals("")) {
                            Toast.makeText(AuthenticationActivity.this, "Please enter Email", Toast.LENGTH_LONG).show();
                        } else if (!email.contains("@") || !email.contains(".")) {
                            Toast.makeText(AuthenticationActivity.this, "Please enter valid Email", Toast.LENGTH_LONG).show();
                        }else if(password.equals("")){
                            Toast.makeText(AuthenticationActivity.this, "Please enter password", Toast.LENGTH_LONG).show();

                        }
                        else {
                            signin(email, password);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.txt_forgot:
                et_password.setVisibility(View.GONE);
                forgot.setVisibility(View.GONE);
                 btn_switch.setVisibility(View.GONE);
                btn_login.setText("Reset");

        }
    }

    private void forgotpassword(String email) {
        progressDialog.setMessage("Please Wait..");
        progressDialog.show();

        mAuth.sendPasswordResetEmail(email).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(AuthenticationActivity.this, "We have sent you instructions to reset your password!", Toast.LENGTH_SHORT).show();
                    et_password.setVisibility(View.VISIBLE);
                    forgot.setVisibility(View.VISIBLE);
                    btn_switch.setVisibility(View.VISIBLE);
                    btn_login.setText("Continue");
                    progressDialog.dismiss();
                } else {
                    progressDialog.dismiss();
                    et_password.setVisibility(View.VISIBLE);
                    forgot.setVisibility(View.VISIBLE);
                     btn_switch.setVisibility(View.VISIBLE);
                    btn_login.setText("Continue");
                    Toast.makeText(AuthenticationActivity.this, "Failed to send reset email!", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}
