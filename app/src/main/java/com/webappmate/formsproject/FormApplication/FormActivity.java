package com.webappmate.formsproject.FormApplication;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.res.ColorStateList;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.webappmate.formsproject.Model.OptionItem;
import com.webappmate.formsproject.R;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class FormActivity extends AppCompatActivity {


 EditText et_name, et_email, et_mobile, et_addrss, et_createdDate, et_formId;
    TextView clientName, clientEmail, clientAddress, clientMobile;
    DatabaseReference mDatabase;
    LinearLayout linearLayout, linerLayoutChild;
    LinearLayout.LayoutParams lparams;
    int i = 0;

    RadioGroup rgroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);
        /*
        String name = getIntent().getStringExtra("name");
        String email = getIntent().getStringExtra("email");
        String mobile = getIntent().getStringExtra("mobile");
        String address = getIntent().getStringExtra("address");
        String createdDate = getIntent().getStringExtra("createdDate");
        */ String formId = getIntent().getStringExtra("formId");
        mDatabase = FirebaseDatabase.getInstance().getReference();
        linearLayout = (LinearLayout) findViewById(R.id.activity_form);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        lparams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        clientName = new TextView(this);
        //clientName.setGravity(1);
        clientName.setTypeface(Typeface.defaultFromStyle(1));
        clientName.setText("Client Name");
        clientName.setLayoutParams(lparams);
        linearLayout.addView(clientName);
        clientName.setTextSize(20);
        clientName.setTextColor(getResources().getColor(R.color.white));
        clientName.setPadding(7, 10, 0, 10);

/*        et_name = new EditText(this);
        et_name.setText(name);
        et_name.setLayoutParams(lparams);
        et_name.setGravity(1);
        et_name.setTextSize(15);
        et_name.setPadding(20, 10, 20, 10);
        et_name.setBackgroundResource(R.drawable.edittext_background);
        linearLayout.addView(et_name);

        clientEmail = new TextView(this);
        //clientName.setGravity(1);
        clientEmail.setTypeface(Typeface.defaultFromStyle(1));
        clientEmail.setText("Client Email");
        clientEmail.setLayoutParams(lparams);
        linearLayout.addView(clientEmail);
        clientEmail.setTextSize(20);
        clientEmail.setTextColor(getResources().getColor(R.color.white));
        clientEmail.setPadding(7, 10, 0, 10);

        et_email = new EditText(this);
        et_email.setText(email);
        et_email.setLayoutParams(lparams);
        et_email.setGravity(1);
        et_email.setTextSize(15);
        et_email.setPadding(20, 10, 20, 10);
        et_email.setBackgroundResource(R.drawable.edittext_background);
        linearLayout.addView(et_email);

        clientMobile = new TextView(this);
        //clientName.setGravity(1);
        clientMobile.setTypeface(Typeface.defaultFromStyle(1));
        clientMobile.setText("Client Mobile Number");
        clientMobile.setLayoutParams(lparams);
        linearLayout.addView(clientMobile);
        clientMobile.setTextSize(20);
        clientMobile.setTextColor(getResources().getColor(R.color.white));
        clientMobile.setPadding(7, 10, 0, 10);

        et_mobile = new EditText(this);
        et_mobile.setText(mobile);
        et_mobile.setLayoutParams(lparams);
        et_mobile.setGravity(1);
        et_mobile.setTextSize(15);
        et_mobile.setPadding(20, 10, 20, 10);
        et_mobile.setBackgroundResource(R.drawable.edittext_background);
        linearLayout.addView(et_mobile);

        clientAddress = new TextView(this);
        //clientName.setGravity(1);
        clientAddress.setTypeface(Typeface.defaultFromStyle(1));
        clientAddress.setText("Client Address");
        clientAddress.setLayoutParams(lparams);
        linearLayout.addView(clientAddress);
        clientAddress.setTextSize(20);
        clientAddress.setTextColor(getResources().getColor(R.color.white));
        clientAddress.setPadding(7, 10, 0, 10);

        et_addrss = new EditText(this);
        et_addrss.setText(address);
        et_addrss.setLayoutParams(lparams);
        et_addrss.setGravity(1);
        et_addrss.setTextSize(15);
        et_addrss.setPadding(20, 10, 20, 10);
        et_addrss.setBackgroundResource(R.drawable.edittext_background);
        linearLayout.addView(et_addrss);*/

        mDatabase.child(formId).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                Map<String, String> map = (Map<String, String>) dataSnapshot.getValue();
                String keyy = dataSnapshot.getKey();
                linearLayout(keyy);
                Map<String, String> optionMap;
                List<OptionItem> optionItemList = new ArrayList<OptionItem>();

                String quest = map.get("Question");
                textview(quest);
                String type = map.get("type");
                if (type.equals("radioButton")) {
                    rgroup = new RadioGroup(getApplicationContext());
                    linerLayoutChild.addView(rgroup);

                    for (Map.Entry entry : map.entrySet()) {
                        if (entry.getKey().equals("options")) {
                            optionMap = (Map<String, String>) entry.getValue();
                            Iterator iterator = optionMap.keySet().iterator();
                            while (iterator.hasNext()) {
                                String key = (String) iterator.next();
                                String value = optionMap.get(key);
                                radioButton(value, i++);
                            }

                        }
                    }

                } else if (type.equals("checkBox")) {
                    for (Map.Entry entry : map.entrySet()) {
                        if (entry.getKey().equals("options")) {
                            optionMap = (Map<String, String>) entry.getValue();
                            Iterator iterator = optionMap.keySet().iterator();
                            while (iterator.hasNext()) {
                                String key = (String) iterator.next();
                                String value = optionMap.get(key);
                                checkBoxes(value, i++);
                            }

                        }
                    }
                } else if (type.equals("datePicker")) {
                    editTextDatePicker();

                } else if (type.equals("editText")) {

                } else if (type.equals("dropDown")) {
                    for (Map.Entry entry : map.entrySet()) {

                        if (entry.getKey().equals("options")) {
                            ArrayList<String> list = null;
                            list = new ArrayList<String>();
                            optionMap = (Map<String, String>) entry.getValue();
                            Iterator iterator = optionMap.keySet().iterator();
                            while (iterator.hasNext()) {
                                String key = (String) iterator.next();
                                String value = optionMap.get(key);
                                list.add(value);
                                Log.d("optionsMap", value);
                            }
                            spinerList(list, i++);
                        }

                    }
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void textview(String question) {
        TextView clientAddress = new TextView(this);
        //clientName.setGravity(1);
        clientAddress.setTypeface(Typeface.defaultFromStyle(1));
        clientAddress.setText(question);
        clientAddress.setLayoutParams(lparams);
        linerLayoutChild.addView(clientAddress);
        clientAddress.setTextSize(20);
        clientAddress.setTextColor(getResources().getColor(R.color.white));
        clientAddress.setPadding(7, 10, 0, 10);
    }

    private void radioButton(String option, int i) {
        RadioButton rbn = new RadioButton(this);
        rbn.setId(i);
        rbn.setText(option);
        rbn.setTextColor(getResources().getColor(R.color.white));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            rbn.setButtonTintList(ColorStateList.valueOf(getResources().getColor(R.color.white)));
        }
        rgroup.addView(rbn);
    }

    private void checkBoxes(String option, int i) {
        CheckBox cb = new CheckBox(getApplicationContext());
        cb.setText(option);
        cb.setId(i);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            cb.setButtonTintList(ColorStateList.valueOf(getResources().getColor(R.color.white)));
        }
        linerLayoutChild.addView(cb);
    }

    private void editTextDatePicker() {
        final EditText et_datepicker = new EditText(this);
        final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        et_datepicker.setLayoutParams(lparams);
        et_datepicker.setPadding(20, 10, 20, 10);
        et_datepicker.setHint("Pick your Date");
        et_datepicker.setFocusable(false);
        et_datepicker.setGravity(1);
        et_datepicker.setBackgroundResource(R.drawable.edittext_background);
        Calendar newCalendar = Calendar.getInstance();
        final DatePickerDialog datepicker = new DatePickerDialog(this, R.style.dialogTheme, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                et_datepicker.setText(dateFormatter.format(newDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        et_datepicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datepicker.show();
            }
        });
        linerLayoutChild.addView(et_datepicker); // here lineaLayoutChild is the object for your layout where you want to add.
    }

    private void spinerList(ArrayList<String> options, int i) {
        Spinner spinner = new Spinner(this);
        spinner.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, options));
        // linearLayout.addView(linerLayoutChild);
        linerLayoutChild.addView(spinner);

    }

    private void linearLayout(String tag) {
        linerLayoutChild = new LinearLayout(getApplicationContext());
        linerLayoutChild.setOrientation(LinearLayout.VERTICAL);
        linerLayoutChild.setTag(tag);
        linearLayout.addView(linerLayoutChild);
    }


}


















