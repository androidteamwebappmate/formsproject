package com.webappmate.formsproject.FormApplication;

import android.app.Application;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

/**
 * Created by newuser on 20/1/17.
 */

public class FormsApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this,new Crashlytics());
    }
}
