package com.webappmate.formsproject.FormApplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.RadioGroup;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.webappmate.formsproject.R;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

public class Test extends AppCompatActivity {
DatabaseReference mDatabase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        mDatabase= FirebaseDatabase.getInstance().getReference();
        mDatabase.child("form").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Map<String, String> map = (Map<String, String>) dataSnapshot.getValue();
                Map<String, String> optionMap;
                String quest = map.get("Question");
                String type = map.get("type");
                if (type.equals("radioButton")) {
                    for (Map.Entry entry : map.entrySet()) {
                        if (entry.getKey().equals("options")) {
                            optionMap = (Map<String, String>) entry.getValue();
                            Iterator iterator = optionMap.keySet().iterator();
                            while (iterator.hasNext()) {
                                String key = (String) iterator.next();
                                String value = optionMap.get(key);

                            }

                        }
                    }

                } else if (type.equals("checkBox")) {
                    for (Map.Entry entry : map.entrySet()) {
                        if (entry.getKey().equals("options")) {
                            optionMap = (Map<String, String>) entry.getValue();
                            Iterator iterator = optionMap.keySet().iterator();
                            while (iterator.hasNext()) {
                                String key = (String) iterator.next();
                                String value = optionMap.get(key);

                            }

                        }
                    }
                } else if (type.equals("datePicker")) {


                } else if (type.equals("editText")) {

                } else if (type.equals("dropDown")) {
                    for (Map.Entry entry : map.entrySet()) {

                        if (entry.getKey().equals("options")) {
                            ArrayList<String> list = null;
                            list = new ArrayList<String>();
                            optionMap = (Map<String, String>) entry.getValue();
                            Iterator iterator = optionMap.keySet().iterator();
                            while (iterator.hasNext()) {
                                String key = (String) iterator.next();
                                String value = optionMap.get(key);
                                list.add(value);

                            }

                        }

                    }
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
